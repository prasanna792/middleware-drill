const fs = require('fs');
const path = require('path');

const logFilePath = path.join(__dirname, 'logFile.log');

const logMiddleware = (req, res, next) => {
    const requestDetails = `${req.method} ${req.url}\n`;
    console.log(requestDetails);

    fs.appendFile(logFilePath, requestDetails, (err) => {
        if (err) {
            console.error(err.message,"error in middleware");
        } else {
            next();
        }
    });

};

module.exports = logMiddleware;
