
const express = require('express');
const rootRoute = require('./routes/rootRoute');
const logMiddleware = require('./logMiddleware'); 
const jsonRoute = require('./routes/jsonRoute');
const textRoute = require('./routes/textRoute');
const logsRoute = require('./routes/logsRoute');

const app = express();

const PORT = process.env.PORT || 8000;

app.use(express.json());
app.use(jsonRoute);
app.use(logMiddleware);
app.use(rootRoute);

app.use(textRoute);
app.use(logsRoute);


app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});



