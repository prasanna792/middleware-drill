const express = require('express');
const router = express.Router();
const jsonData = require('../data/jsonData');


router.get('/json', (req, res) => {
    res.send(jsonData);
});

router.post('/json', (req, res) => {
    let createData = { name: "Renu", city: "East godavari" }
    jsonData.push(createData);
    res.send(jsonData);
});

router.put('/json/:name', (req, res) => {
    try {
        const name = req.params.name;
        if (!name) {
            throw new Error('Name parameter is missing.');
        }

        jsonData[1].name = name;
        res.send(jsonData);
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
});

router.delete('/json/:id', (req, res) => {
    try {
        const id = req.params.id;
        if (!id || isNaN(id)) {
            throw new Error('Invalid ID .');
        }

        const numericId = parseInt(id);
        if (numericId < 0 || numericId >= jsonData.length) {
            throw new Error('Invalid ID value.');
        }

        jsonData.splice(numericId, 1);
        res.send(jsonData);
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
});






module.exports = router;