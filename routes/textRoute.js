const express = require('express');
const router = express.Router();
let stringData = require('../data/textData');



router.get('/text', (req, res) => {
    res.send(stringData);
});

router.post('/text',(req,res) => {
    res.send('text path created text');
});

router.put('/text',(req,res) => {
     stringData = `${stringData} and updated it`;
    res.send(stringData);
});

router.delete('/text',(req,res) => {
    res.send(stringData.slice(0,4));
})


module.exports = router;