
const express = require('express');
const router = express.Router();
const path = require('path');
const fs = require('fs');
const logFilePath = path.join(__dirname,'../logFile.log');

router.get('/logs', (req, res) => {
    fs.readFile(logFilePath, 'utf8', (err, data) => {
        if (err) {
            console.error(err);
            res.status(500).send('Error log file');
        } else {
            res.send(data);
        }
    });
});

module.exports = router;