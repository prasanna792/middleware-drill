const express = require('express');
const router = express.Router();
let textRootData = require('../data/textRootData');



router.get('/', (req, res) => {
    res.send(textRootData);
});

router.post('/', (req, res) => {
    res.send('Root path created text');
});

router.put('/', (req, res) => {
    textRootData = `${textRootData} and updated it`;
    res.send(textRootData);
});

router.delete('/', (req, res) => {
    res.send(textRootData.slice(0, 4));
})


module.exports = router;