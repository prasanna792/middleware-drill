const members = [
    {
        id: 1,
        name: 'lakshmi',
        email: 'lakshmi@gmail.com',
        status: 'active'
    },
    {
        id: 2,
        name: 'prasanna',
        email: 'prasanna@gmail.com',
        status: 'inactive'
    },
    {
        id: 3,
        name: 'sivamma',
        email: 'sivamma@gmail.com',
        status: 'active'
    }
];

module.exports = members;